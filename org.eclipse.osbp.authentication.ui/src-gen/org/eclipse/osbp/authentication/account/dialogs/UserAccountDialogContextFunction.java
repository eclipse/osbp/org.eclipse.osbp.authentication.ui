package org.eclipse.osbp.authentication.account.dialogs;

import com.vaadin.ui.ComponentContainer;
import java.util.HashMap;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IContextFunction;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.osbp.blob.component.BlobConverter;
import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.ecview.core.common.context.IConfiguration;
import org.eclipse.osbp.ecview.core.common.context.IContext;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.eventbroker.EventBrokerMsg;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.VaadinRenderer;
import org.eclipse.osbp.ui.api.contextfunction.IViewEmbeddedProvider;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.statemachine.IDataProvider;
import org.eclipse.osbp.ui.api.statemachine.IStateMachine;
import org.eclipse.osbp.ui.api.statemachine.IStateMachineParticipant;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.xtext.dialogdsl.DialogDSLPackage;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = IContextFunction.class, property = "service.context.key=UserAccount")
@SuppressWarnings("all")
public class UserAccountDialogContextFunction implements IContextFunction {
  @Override
  public Object compute(final IEclipseContext context, final String contextKey) {
    MApplication application = context.get(MApplication.class);
    IEclipseContext appCtx = application.getContext();
    IDSLMetadataService dslMetadataService = appCtx.get(IDSLMetadataService.class);
    appCtx.set("embeddedDialogModel", dslMetadataService.getMetadata("org.eclipse.osbp.authentication.account.dialogs.UserAccount", DialogDSLPackage.Literals.DIALOG));
    IViewEmbeddedProvider provider = ContextInjectionFactory.make(org.eclipse.osbp.vaaclipse.addons.softwarefactory.perspective.DialogProvider.class, appCtx);
    appCtx.set(IViewEmbeddedProvider.class, provider);
    return provider;
  }
}
