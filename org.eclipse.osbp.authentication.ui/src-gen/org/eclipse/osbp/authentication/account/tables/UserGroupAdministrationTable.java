package org.eclipse.osbp.authentication.account.tables;

import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomTable;
import com.vaadin.ui.CustomTable.Align;
import com.vaadin.ui.CustomTable.ColumnCollapseEvent;
import com.vaadin.ui.CustomTable.ColumnCollapseListener;
import com.vaadin.ui.CustomTable.ColumnReorderEvent;
import com.vaadin.ui.CustomTable.ColumnReorderListener;
import com.vaadin.ui.CustomTable.ColumnResizeEvent;
import com.vaadin.ui.CustomTable.ColumnResizeListener;
import com.vaadin.ui.CustomTable.RowHeaderMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import org.eclipse.bpmn2.Task;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.extensions.EventUtils;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.IPresentationEngine;
import org.eclipse.osbp.authentication.account.datamarts.UserGroupDatamart;
import org.eclipse.osbp.bpm.api.BPMOperation;
import org.eclipse.osbp.bpm.api.BPMStatus;
import org.eclipse.osbp.bpm.api.BPMTaskEventType;
import org.eclipse.osbp.bpm.api.BPMTaskSummary;
import org.eclipse.osbp.bpm.api.BPMTaskUserEvent;
import org.eclipse.osbp.bpm.api.IBlipBPMConstants;
import org.eclipse.osbp.bpm.api.IBlipBPMFunctionProvider;
import org.eclipse.osbp.bpm.api.IBlipBPMStartInfo;
import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.eventbroker.EventBrokerMsg;
import org.eclipse.osbp.osgi.hybrid.api.AbstractHybridVaaclipseView;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent.EventDispatcherCommand;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent.EventDispatcherDataTag;
import org.eclipse.osbp.runtime.common.event.IDualData;
import org.eclipse.osbp.runtime.common.event.IEventDispatcher;
import org.eclipse.osbp.runtime.common.event.SelectionStore;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.eclipse.osbp.runtime.web.vaadin.common.data.IBeanSearchServiceFactory;
import org.eclipse.osbp.ui.api.contextfunction.IViewEmbeddedProvider;
import org.eclipse.osbp.ui.api.customfields.IBlobService;
import org.eclipse.osbp.ui.api.datamart.DatamartFilter;
import org.eclipse.osbp.ui.api.datamart.DatamartPrimary;
import org.eclipse.osbp.ui.api.datamart.IDatamartFilterGenerator;
import org.eclipse.osbp.ui.api.datamart.IDatamartFilterGenerator.FilterChangeListener;
import org.eclipse.osbp.ui.api.e4.IE4Table;
import org.eclipse.osbp.ui.api.layout.IViewLayoutManager;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.perspective.IPerspectiveProvider;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService.ThemeResourceType;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.utils.constants.ExtendedDate;
import org.eclipse.osbp.utils.vaadin.SelectUserWindow;
import org.eclipse.osbp.utils.vaadin.ViewLayoutManager;
import org.eclipse.osbp.vaaclipse.common.ecview.api.IECViewSessionHelper;
import org.eclipse.osbp.xtext.action.SelectWorkloadActionEnum;
import org.eclipse.osbp.xtext.action.TableActionEnum;
import org.eclipse.osbp.xtext.blip.BlipItem;
import org.eclipse.osbp.xtext.datamart.common.AEntityDatamart;
import org.eclipse.osbp.xtext.datamart.common.DatamartFilterGenerator;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedAxis;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedCellSet;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedHierarchy;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedLevel;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedMember;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedPosition;
import org.eclipse.osbp.xtext.datamart.common.sql.OperativeDtoContainer;
import org.eclipse.osbp.xtext.table.common.BeanFilterTable;
import org.eclipse.osbp.xtext.table.common.CellSetFilterTable;
import org.eclipse.osbp.xtext.table.common.CellSetIndexedContainer;
import org.eclipse.osbp.xtext.table.common.CellSetPagedFilterTable;
import org.eclipse.osbp.xtext.table.common.CheckboxSelectionCellSetFilterTable;
import org.eclipse.osbp.xtext.table.common.PropertyLookup;
import org.eclipse.osbp.xtext.table.common.TableFilterDecorator;
import org.eclipse.osbp.xtext.table.common.TableFilterGenerator;
import org.eclipse.osbp.xtext.table.common.export.CsvExport;
import org.eclipse.osbp.xtext.table.common.export.ExcelExport;
import org.eclipse.osbp.xtext.table.common.export.PdfExport;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.vaadin.hene.popupbutton.PopupButton;

@SuppressWarnings("all")
public class UserGroupAdministrationTable extends AbstractHybridVaaclipseView implements IUser.UserLocaleListener, IEventDispatcher.Receiver, IE4Table, IDatamartFilterGenerator.FilterChangeListener {
  private static Logger log = org.slf4j.LoggerFactory.getLogger("tables");
  
  private boolean initDone = false;
  
  private VerticalLayout parent;
  
  @Inject
  private IEclipseContext eclipseContext;
  
  @Inject
  private IUser user;
  
  @Inject
  private IEventDispatcher eventDispatcher;
  
  private ArrayList<Integer> coordinateSystem;
  
  private IViewLayoutManager layoutManager;
  
  @Inject
  private IDSLMetadataService dslMetadataService;
  
  @Inject
  private IBlobService blobService;
  
  private UserGroupDatamart datamartInstance;
  
  private IDatamartFilterGenerator filterGenerator;
  
  private boolean moreToLoad = false;
  
  private Component dataComponent;
  
  private boolean mustRefresh = false;
  
  private HashMap<String, PropertyLookup> attributeLookupMap;
  
  private HashMap<Component, ArrayList<ArrayList<Integer>>> tabSheets;
  
  private CellSetIndexedContainer dataSourceContainer;
  
  private OperativeDtoContainer operativeDtoContainer;
  
  private ArrayList<CellSetFilterTable> tables;
  
  @Inject
  private IPresentationEngine renderingEngine;
  
  @Inject
  private IThemeResourceService themeResourceService;
  
  private TableFilterDecorator tableFilterDecorator;
  
  @Inject
  public UserGroupAdministrationTable(final VerticalLayout parent, final IEclipseContext context, final MApplication app) {
    super(parent,context,app);
    
  }
  
  public ArrayList<Integer> getCoordinateSystem() {
    return this.coordinateSystem;
  }
  
  public DerivedCellSet getCellSet(final boolean limited) {
    operativeDtoContainer = null;
    DerivedCellSet cellSet = datamartInstance.getResults(limited, getTaskOperativeDtoClass(), getTaskInitialOperativeDtos());
    moreToLoad = datamartInstance.isMoreToLoad();
    if	(cellSet != null) {
    	operativeDtoContainer = cellSet.getOperativeDtoContainer();
    }
    return cellSet;
    
  }
  
  @Focus
  public void setFocus() {
    if(tables != null && !tables.isEmpty()) {
    	tables.get(0).focus();
    }
  }
  
  @PostConstruct
  public void activate() {
    super.initView();
    user.addUserLocaleListener(this);
    eventDispatcher.addEventReceiver(this);
    filterGenerator.addFilterChangeListener(this);
    
  }
  
  @PreDestroy
  public void deactivate() {
    user.removeUserLocaleListener(this);
    eventDispatcher.removeEventReceiver(this);
    filterGenerator.removeFilterChangeListener(this);
    super.destroyView();
  }
  
  public void createView(final VerticalLayout parent) {
    getContext().set(IE4Table.class, this);
    tables = new ArrayList<CellSetFilterTable>();
    tableFilterDecorator = new TableFilterDecorator(dslMetadataService, user.getLocale());
    // the timeout to begin the filter process after the last key pressed
    tableFilterDecorator.setTextChangeTimeout(500);
    parent.setPrimaryStyleName("osbp");
    parent.setId("parent");
    parent.setSizeFull();
    layoutManager = new ViewLayoutManager();
    layoutManager.init(parent);
    this.parent = parent;
    datamartInstance = new UserGroupDatamart();
    datamartInstance.setUser(user);
    filterGenerator = new DatamartFilterGenerator(datamartInstance, getContext(), false, 10);
    filterGenerator.createUIFilters(layoutManager);
    coordinateSystem = new ArrayList<Integer>();
    attributeLookupMap = new HashMap<>();
    tabSheets = new HashMap<>();
    initDone = true;
    
  }
  
  public void createComponents() {
    if (renderAllowed()){
    	// get the results
    	final DerivedCellSet cellSet = getCellSet(true);
    	if (cellSet == null) {
    		promptSecurityMessage(dslMetadataService.translate(user.getLocale().toLanguageTag(), "securityMessage"), layoutManager.getDataArea());
    		return;
    	} else {
    		layoutManager.getDataArea().removeAllComponents();
    	}
    	getCoordinateSystem().clear();
    	tables.clear();
    	// generate a new result component
    	if (cellSet != null) {
    		if(moreToLoad) {
    			Runnable backgroundFetch = new Runnable() {
    				public void run() {
    				    operativeDtoContainer = null;
    				    final DerivedCellSet cellSetAll = datamartInstance.getResults(false, getTaskOperativeDtoClass(), getTaskInitialOperativeDtos());
    				    if	(cellSet != null) {
    				    	operativeDtoContainer = cellSet.getOperativeDtoContainer();
    				    }
    					for(CellSetFilterTable table:tables) {
    					    dataSourceContainer = new CellSetIndexedContainer(eclipseContext, attributeLookupMap, cellSetAll, coordinateSystem, false);
    						UI.getCurrent().access(new Runnable() {
    							@Override
    							public void run() {
    							    table.setContainerDataSource(dataSourceContainer);
    							    initTable(table, cellSetAll);
    							    filterGenerator.addFilterChangeListener(org.eclipse.osbp.authentication.account.tables.UserGroupAdministrationTable.this);
    							}
    						});
    					}
    				}
    			};
    			new Thread(backgroundFetch).start();
    			moreToLoad = false;
    		} else {
    		    filterGenerator.addFilterChangeListener(org.eclipse.osbp.authentication.account.tables.UserGroupAdministrationTable.this);
    		}
    		// create a multidimensional coordinate system against the cellSet
    		for	(int axis = 0; axis < cellSet.getAxes().size(); axis++) {
    			getCoordinateSystem().add(0);
    		}
    		// remove any previous component
    		if	(dataComponent != null) {
    			layoutManager.getDataArea().removeComponent(dataComponent);
    			dataComponent = null;
    		}
    		if (cellSet.getAxes().size() < 2) {
    			log.error("at least 2 axes from referenced datamart UserGroup are needed to render UserGroupAdministration");
    		} else {
    			dataComponent = createTabSheet(cellSet, cellSet.getAxes().size());
    			if	(dataComponent != null) {
    				dataComponent.setSizeFull();
    				dataComponent.setId("dataComponent");
    				layoutManager.getDataArea().addComponent(dataComponent);
    				layoutManager.getDataArea().setExpandRatio(dataComponent, 1);
    			}
    		}
    	} 
    	else {
    		log.error("referenced datamart UserGroup generates no results");
    	}
    }
    
  }
  
  public void setEnableManualInput(final boolean manualInput) {
    
  }
  
  public void setInput(final Object dto) {
    
  }
  
  private boolean getSelectById() {
    return false;
  }
  
  public Component createTabSheet(final DerivedCellSet cellSet, final Integer axisNo) {
    // either create a recursive tabsheet or a table
    Component component = null;
    if	(axisNo == 2) {
    	component = createTable(cellSet);
    }
    else {		
    	Integer axis = axisNo-1;
    	TabSheet tabsheet = new TabSheet();
    	tabsheet.setSizeFull();
    	tabsheet.addSelectedTabChangeListener(new SelectedTabChangeListener() {
    		@Override
    		public void selectedTabChange(SelectedTabChangeEvent event) {
    			if(tabSheets.containsKey(event.getTabSheet())) {
    				getCoordinateSystem().clear();
    				getCoordinateSystem().addAll(getCoordinates(event.getTabSheet()));
    			}
    		}
    		private Collection<? extends Integer> getCoordinates(TabSheet tabsheet) {
    			if(!(tabsheet.getTab(tabsheet.getSelectedTab()) instanceof TabSheet)) {
    				int index = tabsheet.getTabPosition(tabsheet.getTab(tabsheet.getSelectedTab()));
    				return tabSheets.get(tabsheet).get(index);
    			}
    			return getCoordinates((TabSheet)tabsheet.getTab(tabsheet.getSelectedTab()));
    		}
    	});
    	tabSheets.put(tabsheet, new ArrayList<>());
    	DerivedAxis tabAxis = cellSet.getAxes().get(axis);
    	// create a tab page for all tab axis members
    	int tabNo = 0;
    	for	(DerivedPosition column : tabAxis.getPositions()) {
    		// create the title for the axis
    		String title = null;
    		for (DerivedMember member : column.getMembers()) {
    			if	(title == null) {
    				title = dslMetadataService.translate(user.getLocale().toLanguageTag(), member.getCaption());
    			}
    			else {
    				title += " / "+dslMetadataService.translate(user.getLocale().toLanguageTag(), member.getCaption());
    			}
    		}
    		// position the data to this coordinate
    		getCoordinateSystem().set(axis, tabNo);
    		tabSheets.get(tabsheet).add(new ArrayList<Integer>(getCoordinateSystem()));
    		component = createTabSheet(cellSet, axis);
    		// set the caption
    		if (component != null) {
    			component.setCaption(title);
    			tabsheet.addComponent(component);
        		if (component instanceof CellSetFilterTable) {
    	    		tabsheet.addComponent(((CellSetFilterTable)component));
        		}
    		}
    		tabNo++;
    	}
    	component = tabsheet;
    }
    return component;
  }
  
  public Component createTable(final DerivedCellSet cellSet) {
    final CellSetFilterTable table = new CellSetFilterTable();
    VerticalLayout tableLayout = new VerticalLayout();
    table.setImmediate(true);
    table.setMultiSelect(false);
    table.setColumnCollapsingAllowed(true);
    table.setColumnReorderingAllowed(true);
    table.setId("table");
    table.setRowHeaderMode(RowHeaderMode.HIDDEN);
    table.setFilterDecorator(tableFilterDecorator);
    table.setFilterBarVisible(true);
    table.setSelectable(true);
    table.setSizeFull();
    tableLayout.addComponent(table);
    tables.add(table);
    initTable(table, cellSet);
    return tableLayout;
  }
  
  public void initTable(final CellSetFilterTable table, final DerivedCellSet cellSet) {
     // add attributeLookups for column and row attribute supplements
    attributeLookupMap.put("__USERGROUP__ID__", new PropertyLookup(themeResourceService, dslMetadataService, blobService, user.getLocale())
    	.setCollapseColumn(true)
    );
    // DO NOT MOVE THESE LINES!!!!! The data source container creation has to be before the user properties readings
    dataSourceContainer = new CellSetIndexedContainer(eclipseContext, attributeLookupMap, cellSet, coordinateSystem, false);
    table.setContainerDataSource(dataSourceContainer);
    dataSourceContainer.addExtras(table);
    tableSettingsByUserProperties(table);
    table.addValueChangeListener(new ValueChangeListener() {
    	@Override
    	public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
    		Object msg = null;
    		Object id = null;
    		String target = null;
    		Object value = table.getValue();
    		int selection = -1;
    		if	(value instanceof Integer) {
    			selection = (Integer)value;
    		}
    		else {
    			if ((value instanceof Collection<?>) && (((Collection<?>)value).size() == 1)) {
    				value = ((Collection<?>)value).iterator().next();
        			if	(value instanceof Integer) {
        				selection = (Integer)value;
        			}
    			}
    		}
    		if(selection != -1) {
    			Object idObj = datamartInstance.getPrimaryListKeys("__UserGroup__ID__").get(selection);
    			SelectionStore.putSelectionToPerspectiveContext(getPart(), "org.eclipse.osbp.authentication.account.entities.UserGroup.id", idObj);
    			
    			EventDispatcherEvent evnt = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, "org.eclipse.osbp.authentication.account.entities.UserGroup.id", "org.eclipse.osbp.authentication.account.tables.UserGroupAdministration");
    			evnt.addItem(EventDispatcherDataTag.ID, idObj);
    			eventDispatcher.sendEvent(evnt);
    			// send all refs too
    			for (String alias : datamartInstance.getPrimaryList().keySet()){
    				if (!"__UserGroup__ID__".equals(alias)){
    					evnt = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, datamartInstance.getPrimaryList().get(alias).getEntityName() + "." + datamartInstance.getPrimaryList().get(alias).getAttributeName(), "org.eclipse.osbp.authentication.account.tables.UserGroupAdministration");
    					evnt.addItem(EventDispatcherDataTag.ID, datamartInstance.getPrimaryListKeys(alias).get(selection));
    					eventDispatcher.sendEvent(evnt);
    				}
    			}
    		}
    	}
    });
    table.addColumnCollapseListener(new ColumnCollapseListener(){
    	public void columnCollapseStateChange(ColumnCollapseEvent event) {
    		// Get the ID of the column collapse event
    		Object columnId = event.getPropertyId();
    		Object source = event.getSource();
    		String propertyKey = this.getClass().getEnclosingClass().getCanonicalName() + "." + columnId.toString() + ".collapsed";
    		if (source instanceof CustomTable){
    			if (((CustomTable)source).isColumnCollapsed(columnId)) {
    				user.addToProperties(propertyKey, "true");
    			} else {
    				user.removeFromProperties(propertyKey);
    			}
    		}
    	}
    });
    table.addColumnReorderListener(new ColumnReorderListener(){
    	public void columnReorder(ColumnReorderEvent event) {
    		Object source = event.getSource();
    		String propertyKey = this.getClass().getEnclosingClass().getCanonicalName() + ".columns";
    		if (source instanceof CellSetFilterTable){
    			Map<Object, Integer> visibleColumnsPositions = new HashMap<>();
    			Object[] visibleColumns = ((CellSetFilterTable)source).getVisibleColumns();
    			for (int i = 0; i < visibleColumns.length; i++) {
    				CellSetIndexedContainer container = ((CellSetFilterTable)source).getCellSetIndexedContainer();
    				Object visibleColumnKey = visibleColumns[i];
    				// creates a hash code by the column position members		
    				int membersHash = 0;
    				if (container != null) {
    					DerivedPosition pos = container.getColumnPosition(visibleColumnKey);
    					if ( pos != null ) {
    						List<DerivedMember> members = pos.getMembers();
    						for (DerivedMember member : members) {
    							membersHash += (member.getUniqueName() != null) ? member.getUniqueName().hashCode() : member.hashCode();
    						}
    					}
    				}
    				visibleColumnsPositions.put(visibleColumnKey, membersHash);
    			}
    			if (visibleColumns!=null){
    				user.addToProperties(propertyKey, visibleColumnsPositions);
    			}
    		}
    	}
    });
    table.addColumnResizeListener(new ColumnResizeListener(){
    	public void columnResize(ColumnResizeEvent event) {
    		// Get the new width of the resized column
    		int width = event.getCurrentWidth();
    		
    		// Get the property ID of the resized column
    		Object columnId = event.getPropertyId();
    		user.addToProperties(this.getClass().getEnclosingClass().getCanonicalName() + "." + columnId.toString() + ".width", String.valueOf(width));
    	}
    });
    // Must be immediate to send the resize events immediately
    table.setImmediate(true);
    
  }
  
  public void tableSettingsByUserProperties(final CellSetFilterTable table) {
    // Setting the order of the columns via the visibleColumns list
    Map<Object, Integer> visibleColumnsMap = user.getColumnUtil().getVisibleColumns(this.getClass().getCanonicalName() + ".columns");
    if (visibleColumnsMap != null){
    	boolean unmodifiedVisibleColumns = true;
    	for (Object visibleColumnKey : visibleColumnsMap.keySet()) {
    		CellSetIndexedContainer container = dataSourceContainer;
    		// creates a hash code by the column position members		
    		int membersHash = 0;
    		if (container != null) {
    			DerivedPosition pos = container.getColumnPosition(visibleColumnKey);
    			if ( pos != null ) {
    				List<DerivedMember> members = pos.getMembers();
    				for (DerivedMember member : members) {
    					membersHash += (member.getUniqueName() != null) ? member.getUniqueName().hashCode() : member.hashCode();
    				}
    			}
    		}
    		if (membersHash != visibleColumnsMap.get(visibleColumnKey)) {
    			unmodifiedVisibleColumns = false;
    			break;
    		}
    	}
    	if (unmodifiedVisibleColumns) {
    		try {
    			Object[] visibleColumns = visibleColumnsMap.values().toArray();
    			table.setVisibleColumns(visibleColumns );
    		} catch (IllegalArgumentException e) {
    			// this occurs if a saved column is no longer existing due to model changes
    		}
    	}
    }
    // Setting table columns width from the persisted user properties
    for (Object visibleId : table.getVisibleColumns()) {
    	String widthStr = user.getColumnUtil().getColumnWidth(this.getClass().getCanonicalName() + "." + visibleId.toString() + ".width");
    	if (widthStr!= null) {
    		try {
    			table.setColumnWidth(visibleId, Integer.parseInt(widthStr));
    		} catch (IllegalArgumentException e) {
    			// this occurs if a saved column is no longer existing due to model changes
    		}
    	}
    }
    // Setting table collapsed columns from the persisted user properties
    for (Object propertyId : table.getContainerDataSource().getContainerPropertyIds()) {
    	String collapsedColumn = user.getColumnUtil().getColumnCollapsed(this.getClass().getCanonicalName() + "." + propertyId + ".collapsed");
    	try {
    		if (collapsedColumn != null){
    			table.setColumnCollapsed(propertyId, true);
    		}
    	} catch (IllegalArgumentException e) {
    		// this occurs if a saved column is no longer existing due to model changes
    	}
    }
    
  }
  
  public void dataRefresh() {
    try {
    	// if no current ui found
    	if (UI.getCurrent() == null) {
    		for(CellSetFilterTable table:tables) {
    			UI.setCurrent(table.getUI());
    			break;
    		}
    	}
    	if(dataSourceContainer== null || !dataSourceContainer.updateContainer(getCellSet(true))) {
    		log.debug("cannot update container - full refresh");
    		renderData();
    	}
    	for(CellSetFilterTable table:tables) {
    		table.sanitizeSelection();
    	}
    } catch (Exception e) {
    	StringWriter sw = new StringWriter();
    	e.printStackTrace(new PrintWriter(sw));
    	String exceptionDetails = sw.toString();
    	log.error("table data refresh:{}", exceptionDetails);
    }
  }
  
  public boolean renderAllowed() {
    if (getSelectById()) {
    	for (DatamartFilter filter : datamartInstance.getFilters()) {
    		if(filter.isOptional()) {
    			return true;
    		}
    		if(!filter.getSql().isEmpty()) {
    			return true;
    		}
    	} 
    	return false;
    }
    return true;
  }
  
  @Override
  public void localeChanged(final Locale locale) {
    if(initDone) {
    	tableFilterDecorator.setLocale(locale);
    	for(CellSetFilterTable table:tables) {
    		table.setLocale(locale);
    	}
    	for(String key:attributeLookupMap.keySet()) {
    		attributeLookupMap.get(key).setLocale(locale);
    	}
    	if(dataSourceContainer != null) {
    		dataSourceContainer.setLocale(locale);
    	}
    	layoutManager.setLabelValue(dslMetadataService.translate(locale.toLanguageTag(), "UserGroupAdministration"));
    }
    
  }
  
  @Override
  public void receiveEvent(final EventDispatcherEvent event) {
    switch(event.getCommand()) {
    	case SELECT: 
    		MPerspective perspective = eclipseContext.get(MPerspective.class);
    		if(perspective != null) {
    			ArrayList<String> allowedSenderParts = (ArrayList<String>) eclipseContext.getActive(MPart.class).getTransientData().get(IPerspectiveProvider.EventDispatcherConstants.ACCEPTED_SENDERS);
    			if((event.getPerspective() == null || (perspective != null && event.getPerspective().equals(perspective))) && ((allowedSenderParts != null && allowedSenderParts.contains(event.getSender())) || allowedSenderParts == null)){
    				if(!event.getSender().equals("org.eclipse.osbp.authentication.account.tables.UserGroupAdministration")) {
    					if(filterGenerator.selectItem(event, getSelectById())) {
    						renderData();
    					}
    				}
    			}
    		}
    		break;
    	case SAVE:
    	case DELETE:
    		if(!event.getSender().equals("org.eclipse.osbp.authentication.account.tables.UserGroupAdministration")) {
    			if(event.getTopic().equals("org.eclipse.osbp.authentication.account.entities.UserGroup")){
    				datamartInstance.clearCache();
    				if(datamartInstance.contains(event.getData().get(EventDispatcherDataTag.ID))) {
    					dataRefresh();
    				}
    			}
    		}
    		break;
    	case REFRESH:
    		if(!event.getSender().equals("org.eclipse.osbp.authentication.account.tables.UserGroupAdministration")) {
    			if(event.getTopic().equals("org.eclipse.osbp.authentication.account.entities.UserGroup")){
    				datamartInstance.clearCache();
    				dataRefresh();
    			}
    		}
    		/* must be activated if sure that the filter is to be refreshed
    		if (filterGenerator != null) {
    			filterGenerator.updateFilter();
    		}*/
    		break;
    	}
    
  }
  
  @Override
  public void filterChanged(final DatamartFilter changedFilter) {
    if(changedFilter != null) {
    	MPerspective perspective = eclipseContext.get(MPerspective.class);
    	EventDispatcherEvent evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.SELECT, changedFilter.getName(), "org.eclipse.osbp.authentication.account.tables.UserGroupAdministration");
    	evnt.addData(changedFilter.getSelectedData());
    	eventDispatcher.sendEvent(evnt);
    }
    dataRefresh();
    
  }
}
