/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.ui.login;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthenticationUiUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationUiUtil.class);
	public static final String PORTAL_ID = "1";

	/**
	 * Try to authenticate with the credentials given!<br>
	 * {@link #setAuthenticated(boolean)} will explicit be called!
	 * 
	 * @param portalId
	 * @param userName
	 * @param password
	 * @return true if the user was authenticated successful
	 */
	public static boolean checkPassword(String portalId, String userName, String password) {
		try {
			return InitializationListener.getUserAccessService().authenticate(portalId, userName, password);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
		}
		return false;
	}

	public static Email getEmail() {
		Email email = new SimpleEmail();
		email.setHostName(ProductConfiguration.getEmailServerIp());

		if ( ProductConfiguration.isEmailUseSslOnConnect() ) {
			Integer port=ProductConfiguration.getEmailSmtpPort();
			email.setSslSmtpPort( port.toString() );
			email.setSSLOnConnect(true);
		}
		else {
			email.setSmtpPort(ProductConfiguration.getEmailSmtpPort());
			email.setSSLOnConnect(false);
		}
		
		if (!ProductConfiguration.getAdminEmailUsername().isEmpty()) {
			email.setAuthenticator(new DefaultAuthenticator(ProductConfiguration.getAdminEmailUsername(), ProductConfiguration
					.getAdminEmailPassword()));
		}
		return email;
	}

}
