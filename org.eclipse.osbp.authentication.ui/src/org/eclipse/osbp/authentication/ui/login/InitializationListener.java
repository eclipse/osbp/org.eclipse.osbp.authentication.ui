/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.ui.login;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.osbp.ui.api.useraccess.IUserAccessService;
import org.eclipse.osbp.ui.initialization.AbstractInitializationListener;
import org.eclipse.osbp.ui.initialization.IInitializationNotification;
import org.eclipse.osbp.ui.initialization.IInitializationProvider;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The listener interface for receiving initialization events.
 * The class that is interested in processing a initialization
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addInitializationListener<code> method. When
 * the initialization event occurs, that object's appropriate
 * method is invoked.
 *
 * @see InitializationEvent
 */
@Component
public class InitializationListener extends AbstractInitializationListener {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(InitializationListener.class);

	/** it has to be static, not only session static! */
	public static final InitializationListener INSTANCE = new InitializationListener();

	private static IUserAccessService userAccessService;
	
	private static final Set<AuthenticationProvider> authenticationProviders = new HashSet<AuthenticationProvider>();
	
	public static void addAuthenticationProvider(final AuthenticationProvider provider) {
		authenticationProviders.add(provider);
		INSTANCE.resendLastNotification();
	}
	
	public static void removeAuthenticationProvider(final AuthenticationProvider provider) {
		authenticationProviders.remove(provider);
	}
	
	public static IUserAccessService getUserAccessService() {
		return userAccessService;
	}

	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
	public synchronized void bindUserAccessService(final IUserAccessService userAccessService) {
		InitializationListener.userAccessService = userAccessService;
		LOGGER.debug("IUserAccessService '{}' bound", userAccessService.getClass().getCanonicalName());
	}

	public synchronized void unbindUserAccessService(final IUserAccessService userAccessService) {
		InitializationListener.userAccessService = null;
		LOGGER.debug("IUserAccessService '{}' unbound", userAccessService.getClass().getCanonicalName());
	}
	
	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public synchronized void bindInitializationProvider(final IInitializationProvider provider) {
		LOGGER.debug("IInitializationProvider '{}' bound", provider.getClass().getCanonicalName());
		INSTANCE.addInitializationProvider(provider);
		// --- if initialization has been bounded, maybe new accounts have been generated already ---
		INSTANCE.encryptPasswords();
	}
	
	public synchronized void unbindInitializationProvider(final IInitializationProvider provider) {
		LOGGER.debug("IInitializationProvider '{}' unbound", provider.getClass().getCanonicalName());
		INSTANCE.removeInitializationProvider(provider);
	}

	@Override
	protected void removeAllInitializationProviders() {
		if	(this != INSTANCE) {
			INSTANCE.removeAllInitializationProviders();
		}
		else {
			super.removeAllInitializationProviders();
		}
	}

	@Override
	public synchronized void notifyInitializationStep(IInitializationNotification notification) {
		super.notifyInitializationStep(notification);
		LOGGER.info(notification.getRecommendedProgressTitle(true));
		for	(AuthenticationProvider provider : authenticationProviders) {
			provider.notifyInitializationStep(notification);
		}
	}

	@Override
	public synchronized void notifyInitializationDone(IInitializationNotification notification) {
		super.notifyInitializationDone(notification);
		LOGGER.info(notification.getRecommendedProgressTitle(true));
		LOGGER.info("more initialization to be done? "+hasInitializationToBeDone());
		// --- if initialization has been done, maybe new accounts have been generated ---
		encryptPasswords();
		for	(AuthenticationProvider provider : authenticationProviders) {
			provider.notifyInitializationDone(notification);
		}
	}

	private void encryptPasswords() {
		if	((userAccessService != null)) {
			boolean reEnableLoginUi = false;
			try {
				userAccessService.encryptPasswords();
				reEnableLoginUi = true;
			}
			catch (Exception e) {
				LOGGER.error("could not encrypt any passwords due to exception", e);
				reEnableLoginUi = false;
			}
			LOGGER.debug("AuthenticationProvider encrypt passwords finished - enable login ui:"+reEnableLoginUi);
			for	(AuthenticationProvider provider : authenticationProviders) {
				provider.enableLoginUI(reEnableLoginUi);
			}
		}
	}
}
