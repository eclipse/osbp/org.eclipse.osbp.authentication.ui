/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.ui.login;

import java.time.LocalDate;

import org.apache.commons.validator.routines.EmailValidator;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.osbp.authentication.account.dtos.UserAccountDto;
import org.eclipse.osbp.authentication.providerimpl.UserProtocol;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.vaaclipse.publicapi.authentication.AuthenticationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class RegistrationDialog extends Window {

	private IDSLMetadataService dslMetadataService;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationDialog.class);

	private Panel registerPanel;
	private VerticalLayout layout;

	// private TextField userField;
	// private TextField emailField;
	//
	// private Button registerButton;
	// private Button cancelButton;

	public RegistrationDialog() {
		super();
		setDraggable(false);
		setResizable(false);
		setClosable(false);
		setModal(true);
		layout = new VerticalLayout();
		layout.setMargin(true);
		setContent(layout);

		registerPanel = new Panel();
		registerPanel.setWidth("430px");
		registerPanel.setId("registerPanelArea");
		registerPanel.addStyleName("registerPanelArea os-login");
	}

	public RegistrationDialog init(IEventBroker eventBroker, IDSLMetadataService dslMetadataService, IUser user) {
		LOGGER.debug("RegistrationDialog init");
		setLocale(UI.getCurrent().getLocale());
		this.dslMetadataService = dslMetadataService;

		layout.addComponent(registerPanel);
		layout.setComponentAlignment(registerPanel, Alignment.MIDDLE_CENTER);
		layout.setPrimaryStyleName("osbp");
		registerPanel.setCaption(dslMetadataService.translate(getLocale().toLanguageTag(), "register_caption"));
		registerPanel.setDescription(dslMetadataService.translate(getLocale().toLanguageTag(), "register_caption_tip"));

		// VerticalLayout REGISTERFORM
		VerticalLayout registerForm = new VerticalLayout();
		registerForm.setSizeFull();
		registerForm.setMargin(true);
		registerForm.setId("osbpregisterForm");
		registerForm.addStyleName("loginPanelArea");
		registerPanel.setContent(registerForm);

		// VerticalLayout FULLAREA
		VerticalLayout fullArea = new VerticalLayout();
		fullArea.setSizeFull();
		fullArea.setId("registerFullArea");
		fullArea.addStyleName("registerFullArea");
		registerForm.addComponent(fullArea);

		// HorizontalLayout USERAREA
		HorizontalLayout userArea = new HorizontalLayout();
		userArea.setId("registerUserArea");
		userArea.addStyleName("registerUserArea");
		userArea.setSizeFull();
		userArea.setMargin(true);
		fullArea.addComponent(userArea);

		// VerticalLayout TEXTAREA
		VerticalLayout textArea = new VerticalLayout();
		textArea.setSizeFull();
		userArea.addComponent(textArea);
		userArea.setExpandRatio(textArea, 0.85f);
		userArea.setId("registerTextArea");
		userArea.addStyleName("registerTextArea");

		// VerticalLayout BUTTONAREA
		VerticalLayout buttonArea = new VerticalLayout();
		buttonArea.setId("registerButtonArea");
		buttonArea.addStyleName("registerButtonArea");
		buttonArea.setSizeFull();
		buttonArea.setMargin(true);
		fullArea.addComponent(buttonArea);

		// HorizontalLayout REGISTERAREA
		HorizontalLayout registerArea = new HorizontalLayout();
		registerArea.setId("registerregisterArea");
		registerArea.addStyleName("registerregisterArea");
		registerArea.setSizeFull();
		buttonArea.addComponent(registerArea);

		// VerticalLayout COPYRIGHTAREA
		VerticalLayout copyrightArea = new VerticalLayout();
		copyrightArea.setId("loginCopyrightArea");
		copyrightArea.addStyleName("loginCopyrightArea");
		copyrightArea.setSizeFull();
		registerForm.addComponent(copyrightArea);
		// Label COPYRIGHTFIELD
		Label copyrightField = new Label();
		copyrightField.setSizeFull();
		copyrightArea.addComponent(copyrightField);
		copyrightArea.setComponentAlignment(copyrightField, Alignment.MIDDLE_CENTER);
		String actualYear = String.valueOf(LocalDate.now().getYear());
		copyrightField.setValue(dslMetadataService.translate(getLocale().toLanguageTag(), "copyright") + actualYear);

		// Text field USERNAME
		TextField userNameField = new TextField();
		userNameField.setSizeFull();
		textArea.addComponent(userNameField);
		userNameField.setValue((user.getUserName() != null) ? user.getUserName() : "");
		userNameField.setInputPrompt(dslMetadataService.translate(getLocale().toLanguageTag(), "username"));
		userNameField.setDescription(dslMetadataService.translate(getLocale().toLanguageTag(), "username_tip"));
		userNameField.focus();

		// Password field PASSWORD
		PasswordField passwordField = new PasswordField();
		passwordField.setSizeFull();
		textArea.addComponent(passwordField);
		passwordField.setInputPrompt(dslMetadataService.translate(getLocale().toLanguageTag(), "password"));
		passwordField.setDescription(dslMetadataService.translate(getLocale().toLanguageTag(), "password_tip"));

		// Password field PASSWORD_VERIFIED
		PasswordField passwordVerifyingField = new PasswordField();
		passwordVerifyingField.setSizeFull();
		textArea.addComponent(passwordVerifyingField);
		passwordVerifyingField.setInputPrompt(dslMetadataService.translate(getLocale().toLanguageTag(), "passwordVerifying"));
		passwordVerifyingField.setDescription(dslMetadataService.translate(getLocale().toLanguageTag(), "passwordVerifying_tip"));

		// Text field EMAIL
		TextField emailField = new TextField();
		emailField.setSizeFull();
		textArea.addComponent(emailField);
		emailField.setInputPrompt(dslMetadataService.translate(getLocale().toLanguageTag(), "email"));
		emailField.setDescription(dslMetadataService.translate(getLocale().toLanguageTag(), "email_tip"));

		// Button REGISTER
		Button registerButton = new Button();
		registerButton.setSizeFull();
		registerArea.addComponent(registerButton);
		registerButton.setCaption(dslMetadataService.translate(getLocale().toLanguageTag(), "register_new_user"));
		registerButton.setDescription(dslMetadataService.translate(getLocale().toLanguageTag(), "register_tip"));
		registerButton.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
				if (createNewUser(userNameField, emailField, passwordField, passwordVerifyingField)) {
					eventBroker.send(AuthenticationConstants.Events.Authentication.registration, user);
					closeThisWindow();
				}
			}

		});

		// Button CANCEL
		Button cancelButton = new Button();
		cancelButton.setSizeFull();
		registerArea.addComponent(cancelButton);
		cancelButton.setCaption(dslMetadataService.translate(getLocale().toLanguageTag(), "cancel"));
		cancelButton.setDescription(dslMetadataService.translate(getLocale().toLanguageTag(), "cancel_register_tip"));
		cancelButton.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
				closeThisWindow();
			}

		});
		return this;
	}

	private boolean createNewUser(TextField userNameField, TextField emailField, PasswordField passwordField,
			PasswordField passwordVerifyingField) {
		if (userExists(userNameField.getValue())) {
			Notification.show(dslMetadataService.translate(getLocale().toLanguageTag(), "user_exist_message"));
			userNameField.focus();
			return false;
		} else if (passwordField == null || passwordField.isEmpty()) {
			Notification.show(dslMetadataService.translate(getLocale().toLanguageTag(), "password_is_empty_message"));
			passwordField.focus();
			return false;
		} else if (passwordVerifyingField == null || passwordVerifyingField.isEmpty()) {
			Notification.show(dslMetadataService.translate(getLocale().toLanguageTag(), "password_verifying_is_empty_message"));
			passwordVerifyingField.focus();
			return false;
		} else if (emailField == null || emailField.isEmpty()) {
			Notification.show(dslMetadataService.translate(getLocale().toLanguageTag(), "email_is_empty_message"));
			emailField.focus();
			return false;
		} else if (!EmailValidator.getInstance(true).isValid(emailField.getValue())) {
			Notification.show(dslMetadataService.translate(getLocale().toLanguageTag(), "email_is_incorrect_message"));
			emailField.focus();
			return false;
		} else if (passwordField != null && passwordVerifyingField != null
				&& !passwordField.getValue().equals(passwordVerifyingField.getValue())) {
			Notification.show(dslMetadataService.translate(getLocale().toLanguageTag(), "passwords_not_equal_message"));
			passwordField.focus();
			passwordField.clear();
			passwordVerifyingField.clear();
			return false;
		} else {
			UserAccountDto userDto = new UserAccountDto();
			userDto.setUserName(userNameField.getValue());
			userDto.setEmail(emailField.getValue());
			userDto.setNotRegistered(true);
			String encryptedPassword = InitializationListener.getUserAccessService().encryptPassword(passwordField.getValue());
			userDto.setPassword(encryptedPassword);
			UserProtocol.getDtoUserAccountDtoService().update(userDto);
			Notification.show(dslMetadataService.translate(getLocale().toLanguageTag(), "registration_success_message"));
			return true;
		}
	}

	private boolean userExists(String userName) {
		return InitializationListener.getUserAccessService().checkNotLoggedInUsernameExists(userName);
	}

	private void closeThisWindow() {
		close();
	}
}
